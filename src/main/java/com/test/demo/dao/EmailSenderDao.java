package com.test.demo.dao;


import com.sendgrid.Response;
import com.test.demo.model.SenderEmail;

public interface EmailSenderDao {
    Response sendEmail(SenderEmail email);
}
