package com.test.demo.controller;

import com.sendgrid.*;
import com.test.demo.model.MailStatus;
import com.test.demo.model.SenderEmail;
import com.test.demo.service.SendGridEmailServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@CrossOrigin(origins = { "*", "http://localhost:3000" })
@RestController
public class EmailController {
	
	@Autowired
	SendGridEmailServiceImpl sendEmailService;
	
     
     @CrossOrigin(origins = { "https://localhost:3000" },allowedHeaders = "*")
    @PostMapping(value="/api/sendmail",produces = MediaType.APPLICATION_JSON_VALUE,
    		consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}) 
    @ResponseBody
    public ResponseEntity<MailStatus> sendEmailWithSendGrid(@RequestBody  SenderEmail email) {
    	 
    	 
    	MailStatus mailStatus = new MailStatus();
    	        try {
    	        	sendEmailService.sendEmail(email);
    	        
   		  mailStatus.setStatusMessage("email was successfully send");
    		 mailStatus.setStatusCode(200);
    		 return new ResponseEntity<MailStatus>(mailStatus, HttpStatus.OK);
    	 }catch (Exception e) {
			// TODO: handle exception
    		// return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
         return new ResponseEntity(HttpStatus.BAD_REQUEST);
    	//return "email was successfully send";
    }
     @GetMapping("/emailStatus")
     public String emailStatus() {
         return "emailStatus from";
     }
}
