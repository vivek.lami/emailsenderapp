package com.test.demo.model;

/**
 * Pojo class to hold user request data.
 * 
 * 
 *
 */
public class SenderEmail {

	private String toEmail;
	private String fromEmail;
	private String ccEmail;
	private String bCcEmail;
	private String emailSubject;
	private String message;
	


	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}



	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getbCcEmail() {
		return bCcEmail;
	}
	public void setbCcEmail(String bCcEmail) {
		this.bCcEmail = bCcEmail;
	}
	
	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}
	
	public String getCcEmail() {
		return ccEmail;
	}
	
}
