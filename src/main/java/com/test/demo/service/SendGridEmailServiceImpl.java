package com.test.demo.service;

import com.sendgrid.*;
import com.test.demo.constants.AppConstants;
import com.test.demo.constants.SENDGRID_RESPONSECODE;
import com.test.demo.dao.EmailSenderDao;
import com.test.demo.model.SenderEmail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

@Service
public class SendGridEmailServiceImpl implements EmailSenderDao {

	@Value("${templateId}")
    private String EMAIL_TEMPLATE_ID;
	@Value("$mailgun.api-key")
	private String MAIL_GUN_API_KEY;
    private static final Log LOG = LogFactory.getLog(SendGridEmailServiceImpl.class);

    private SendGrid sendGridClient;

    @Autowired
    public SendGridEmailServiceImpl(SendGrid sendGridClient) {
        this.sendGridClient = sendGridClient;
    }

    public Response sendEmail(SenderEmail senderemail) {
    	Response response = null;
    	
    	try {
			 response = emailVaiSendGrid(senderemail.getFromEmail(), senderemail.getToEmail(), 
					senderemail.getEmailSubject(), new Content("text/html", senderemail.getMessage()));
			 if(SENDGRID_RESPONSECODE.BADREQUEST.getValue() == response.getStatusCode()
					 || SENDGRID_RESPONSECODE.SERVER_UNAVAILABLE.getValue() == response.getStatusCode()
					 || SENDGRID_RESPONSECODE.UNAUTHORIZED.getValue() == response.getStatusCode())
					  {
				 System.out.print("mail gun");
				 emailVaiMailGun(senderemail);
			 }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return response;
    }
    private Response emailVaiSendGrid(String fromM, String toM, String subjectM, Content content) throws IOException {
        Email from   = new Email(fromM);
        String subject = subjectM;
        Email to = new Email(toM);
        Mail mail = new Mail(from, subject, to, content);

        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = this.sendGridClient.api(request);
            LOG.info("Status code: "+ response.getStatusCode() + " Body: "
                    + response.getBody() + " Headers: " + response.getHeaders());
            return response;
        } catch (IOException ex) {
            LOG.error("Error sending mail: " + ex.getMessage());
            throw ex;
        }
    }
    
    private net.sargue.mailgun.Response emailVaiMailGun(SenderEmail email) { 
    	net.sargue.mailgun.Response mailGunResposne = null;
		   mailGunResposne = sendSimpleMessage(email);
    	return mailGunResposne;
    }
    
	public  net.sargue.mailgun.Response sendSimpleMessage(SenderEmail email) {
		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter(AppConstants.API, MAIL_GUN_API_KEY));
		WebResource webResource = client.resource(AppConstants.SANDBOX_MAILGUN_URL);
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		formData.add("from", AppConstants.MAILGUN_SANDBOX_FROM_EMAIL);
		formData.add("to", "vivek sharma <vive_18@yahoo.com>");
		formData.add("subject", email.getEmailSubject());
		formData.add("text", "Congratulations vivek sharma, you just sent an email with Mailgun!  You are truly awesome!");
	return webResource.type(javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED_TYPE).
			post(net.sargue.mailgun.Response.class, formData);
	}
	
	
}
