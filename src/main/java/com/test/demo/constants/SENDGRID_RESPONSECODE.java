package com.test.demo.constants;

public enum SENDGRID_RESPONSECODE {
	OK(200),
	ACCEPTED(202), 
	BADREQUEST(400),
	UNAUTHORIZED(401),
	TOO_MANY_REQUESTS(429),
	SERVER_UNAVAILABLE(500);
	
	private int value;  
	private SENDGRID_RESPONSECODE(int value){  
		this.value=value;  
	} 
	public int getValue() {
		return value;
	}
}
