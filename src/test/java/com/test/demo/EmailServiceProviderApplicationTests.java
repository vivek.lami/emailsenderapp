package com.test.demo;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import static org.mockito.Mockito.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

@SpringBootTest
class EmailServiceProviderApplicationTests {

	@Value("$spring.sendgrid.api-key")
	private String SENDGRID_API_KEY;
	@Value("$mailgun.api-key")
	private String MAIL_GUN_API_KEY;
	@Test
	void contextLoads() {
	}
	
	@Test
	  public void testemail_post() throws IOException {
	    SendGrid sg = new SendGrid("SENDGRID_API_KEY", true);
	    sg.setHost("localhost:8080");
	    sg.addRequestHeader("X-Mock", "200");
	    
	    Email from   = new Email("vivek.lami@gmail.com");
      String subject = "This is Test case for SendGrid";
      Email to = new Email("vive_18@yahoo.com");
      Mail mail = new Mail(from, subject, to, new Content("text/plain", "Hay there testing for SendGrid"));

	    Request request = new Request();
	    request.setMethod(Method.POST);
	    request.setEndpoint("mail/send");
	    request.setBody(mail.build());
	    Response response = sg.api(request);
	    Assert.assertEquals(200, response.getStatusCode());
	  }
	
	@Test
	  public void testInitialization() {
	    SendGrid sg = new SendGrid(SENDGRID_API_KEY);
	    Assert.assertEquals(sg.getHost(), "api.sendgrid.com");
	    Assert.assertEquals(sg.getVersion(), "v3");
	    Map<String,String> requestHeaders = buildDefaultHeaders();
	    Assert.assertEquals(sg.getRequestHeaders(), requestHeaders);
	  }
	
	public Map<String,String> buildDefaultHeaders() {
	    SendGrid sg = new SendGrid(SENDGRID_API_KEY);
	    Map<String,String> requestHeaders = new HashMap<String, String>();
	    requestHeaders.put("Authorization", "Bearer " + SENDGRID_API_KEY);
	    String USER_AGENT = "sendgrid/" + sg.getLibraryVersion() + ";java";
	    requestHeaders.put("User-Agent", USER_AGENT);
	    requestHeaders.put("Accept", "application/json");
	    return requestHeaders;
	  }
	
	


}
